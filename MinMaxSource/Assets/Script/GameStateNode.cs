﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameStateNode : Node<int[,]>
{   
    public GameStateNode gsp;
    public List<GameStateNode> gsc = new List<GameStateNode>();
    public bool usingO;
    public bool ownTurn;
    public int value;
}
