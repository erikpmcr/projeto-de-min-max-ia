﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    //float baseLoadTime = 1f; //valor nunca é usado
    public GameObject systemManager;

    private void Start()
    {
        if (systemManager == null)
        {
            systemManager = GameObject.Find("SystemManager");
        }

    }

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ReturnScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void LoadPanel(GameObject obj)
    {
        obj.SetActive(true);
    }

    public void QuitPanel(GameObject obj)
    {
        obj.SetActive(false);
    }

    public void LoadSpecificScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void LoadSpecificScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void Restart()
    {
        //gameManager.gameObject.GetComponent<GameManager>()._audio.PlayMusic();
        Time.timeScale = 1;
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Game Exited");
    }
}
