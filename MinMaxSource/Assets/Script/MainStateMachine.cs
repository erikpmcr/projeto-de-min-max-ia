﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SerializeField]
public enum state {start, wait, turnO, turnX, player, aiTurn, buildTree, end, reset };

[SerializeField]
public enum player { nil, P1, P2};

public class MainStateMachine : MonoBehaviour
{
    public bool p1O = true, p2O, p2AI = true, trigger;
    [SerializeField]
    public int[,] matrix = new int[3,3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };  //-1 = O 1 = X
    public Text[,] t_Matrix = new Text[3, 3];
    public bool canPlay;
    public string s_Turn;
    public Text t_Turn;
    public player currentPlayer;
    public state stateMac = state.start;
    public List<int[,]> victoryStatesMultiplier = new List<int[,]>();
    // Start is called before the first frame update
    void Start()
    {
        victoryStatesMultiplier.Add(new int[3, 3] { { 1, 0, 0 }, { 1, 0, 0 }, { 1, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { -1, 0, 0 }, { -1, 0, 0 }, { -1, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, -1, 0 }, { 0, -1, 0 }, { 0, -1, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, 1 }, { 0, 0, 1 }, { 0, 0, 1 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, -1 }, { 0, 0, -1 }, { 0, 0, -1 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, -1 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, 1 } ,{ 0, 1, 0 }, { 1, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, -1 }, { 0, -1, 0 }, { -1, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 1, 1, 1 }, { 0, 0, 0 }, { 0, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { -1, -1, -1 }, { 0, 0, 0 }, { 0, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, 0 }, { 1, 1, 1 },  { 0, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, 0 }, { -1, -1, -1 },  { 0, 0, 0 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 1, 1, 1 } });
        victoryStatesMultiplier.Add(new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { -1, -1, -1 } });
        s_Turn = "Vez do usuario:";

    }

    // Update is called once per frame
    void Update()
    {
        if (trigger)
        {
            trigger = false;
            switch (stateMac)
            {
                case state.start:
                    matrix = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

                    if (currentPlayer == player.nil)
                    {
                        float temp = Random.Range(0f, 1f);
                        if (temp > .5)
                        {
                            currentPlayer = player.P1;
                        }
                        else
                        {
                            currentPlayer = player.P2;
                        }
                    }
                    setMatrix();
                    stateMac = state.wait;
                    t_Turn = GameObject.Find("t_Turn").GetComponent<Text>();
                    t_Turn.text = s_Turn + ((currentPlayer == player.P1) ? ("Jogador 1") : ((p2AI) ? ("AI") : ("Jogador 2")));
                    buildMatrix();
                    trigger = true;
                    break;

                case state.wait:
                    if (checkVictory(matrix))
                    {
                        t_Turn.text = s_Turn + ((currentPlayer == player.P1) ? ("Jogador 1 Ganhou") : ((p2AI) ? ("AI Ganhou") : ("Jogador 2  Ganhou")));
                        stateMac = state.end;
                    }
                    else if (checkDraw(matrix))
                    {
                        t_Turn.text = s_Turn + ("Empate");
                        stateMac = state.end;
                    }
                    else
                    {
                        if (currentPlayer == player.P1)
                        {
                            if (p1O)
                            {
                                stateMac = state.turnO;
                            }
                            else
                            {
                                stateMac = state.turnX;
                            }
                        }
                        if (currentPlayer == player.P2)
                        {
                            if (p2O)
                            {
                                stateMac = state.turnO;
                            }
                            else
                            {
                                stateMac = state.turnX;
                            }
                        }

                        trigger = true;
                    }
                    break;
                case state.turnO:
                    if (currentPlayer == player.P1)
                    {
                        stateMac = state.player;
                        
                    }
                    if (currentPlayer == player.P2)
                    {
                        if (p2AI)
                        {
                            stateMac = state.aiTurn;
                        }
                        else
                        {
                            stateMac = state.player;
                        }
                    }
                    t_Turn.text = s_Turn + ((currentPlayer == player.P1) ? ("Jogador 1 - O") : ((p2AI) ? ("AI - O") : ("Jogador 2 - O")));
                    trigger = true;
                    break;
                case state.turnX:
                    if (currentPlayer == player.P1)
                    {
                        stateMac = state.player;
                    }
                    if (currentPlayer == player.P2)
                    {
                        if (p2AI)
                        {
                            stateMac = state.aiTurn;
                        }
                        else
                        {
                            stateMac = state.player;
                        }
                    }
                    t_Turn.text = s_Turn + ((currentPlayer == player.P1) ? ("Jogador 1 - X") : ((p2AI) ? ("AI - X") : ("Jogador 2 - X")));
                    trigger = true;
                    break;
                case state.player:
                    canPlay = true;
                    buildMatrix();
                    stateMac = state.wait;
                    break;
                case state.aiTurn:
                    stateMac = state.buildTree;
                    trigger = true;
                    break;
                case state.buildTree:
                    GameStateNode root = new GameStateNode();
                    root.usingO = p2O;
                    root.ownTurn = true;
                    root.data = matrix;
                    int[,] tempMat = treeBuilder(root, (p2O ? (-1) : (1)));
                    matrix = addMatrix(matrix, tempMat);
                    buildMatrix();
                    trigger = true;
                    currentPlayer = player.P1;
                    stateMac = state.wait;
                    break;
                case state.end:
                    
                    break;
                case state.reset:
                    stateMac = state.start;
                    break;
                default:
                    p1O = true;
                    p2O = false;
                    p2AI = true;
                    trigger = true;
                    matrix = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
                    stateMac = state.start;
                    break;
            }
        }
    }
    
    void setMatrix()
    {
        for(int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                if(t_Matrix[k,l] == null)
                {
                    if (k == 0 && l == 0)
                    {
                        if (GameObject.Find("b_TopLeft").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_TopLeft").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_TopLeft not found");
                        }
                    }
                    if (k == 0 && l == 1)
                    {
                        if (GameObject.Find("b_TopMid").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_TopMid").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_TopMid not found");
                        }
                    }
                    if (k == 0 && l == 2)
                    {
                        if (GameObject.Find("b_TopRight").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_TopRight").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_TopRight not found");
                        }
                    }
                    if (k == 1 && l == 0)
                    {
                        if (GameObject.Find("b_MidLeft").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_MidLeft").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_MidLeft not found");
                        }
                    }
                    if (k == 1 && l == 1)
                    {
                        if (GameObject.Find("b_MidMid").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_MidMid").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_MidMid not found");
                        }

                    }
                    if (k == 1 && l == 2)
                    {
                        if (GameObject.Find("b_MidRight").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_MidRight").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_MidRight not found");
                        }
                    }
                    if (k == 2 && l == 0)
                    {
                        if (GameObject.Find("b_BotLeft").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_BotLeft").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_BotLeft not found");
                        }
                    }
                    if (k == 2 && l == 1)
                    {
                        if (GameObject.Find("b_BotMid").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_BotMid").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_BotMid not found");
                        }
                    }
                    if (k == 2 && l == 2)
                    {
                        if (GameObject.Find("b_BotRight").transform.GetChild(0).gameObject != null)
                        {
                            t_Matrix[k, l] = GameObject.Find("b_BotRight").transform.GetChild(0).gameObject.GetComponent<Text>();
                        }
                        else
                        {
                            Debug.LogWarning("b_BotRight not found");
                        }
                    }
                }
            }
        }
    }

    int[,] addMatrix(int[,] a, int[,] b)
    {
        int[,] tempA = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                tempA[k, l] = a[k, l] + b[k, l];
            }
        }
        return tempA;
    }
    void buildMatrix()
    {
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                if (t_Matrix[k, l] != null)
                {
                    if(matrix[k,l] == 0)
                    {
                        t_Matrix[k, l].text = "";
                    }
                    else if(matrix[k, l] == -1)
                    {
                        t_Matrix[k, l].text = "O";
                    }
                    else if (matrix[k, l] == 1)
                    {
                        t_Matrix[k, l].text = "X";
                    }
                }
            }
        }
    }


    public bool checkVictory(int [,] mat)
    {
        bool temp = false;
        foreach (int[,] i in victoryStatesMultiplier)
        {
            int[,] tempIM = i;
            bool breakOff = false;
            for (int k = 0; k < 3 && !breakOff; k++)
            {
                for (int l = 0; l < 3 && !breakOff; l++)
                {
                    if(tempIM[k, l] == 0)
                    {
                        tempIM[k, l] = tempIM[k, l] * mat[k, l];
                    }
                    else
                    {
                        if (tempIM[k, l] != mat[k, l])
                        {
                            breakOff = true;
                        }
                    }
                    
                }
            }
            if (!breakOff)
            {
                temp = true;
            }
        }
        return temp;
    }

    public bool checkDraw(int[,] mat)
    {
        bool temp = true;
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                if (mat[k, l] == 0)
                {
                    temp = false;
                }
            }
        }
        return temp;
    }

    public int checkDistanceToVictory(int[,] mat)
    {
        int trueDist = 3;
        bool temp = false;
        foreach (int[,] i in victoryStatesMultiplier)
        {
            int dist = 3;
            int[,] tempIM = i;
            bool breakOff = false;

            for (int k = 0; k < 3 && !breakOff; k++)
            {
                for (int l = 0; l < 3 && !breakOff; l++)
                {
                    if (tempIM[k, l] == 0)
                    {
                        tempIM[k, l] = tempIM[k, l] * mat[k, l];
                    }
                    else
                    {
                        if (tempIM[k, l] != mat[k, l])
                        {
                            dist--;
                        }
                    }

                }
            }
            if (dist < trueDist)
            {
                trueDist = dist;
            }
        }
        return trueDist;
    }

    int[,] treeBuilder(GameStateNode root, int type)
    {
        bool done = false;
        int depth = 2;
        int curDepth = 0;
        bool max = true;

        GameStateNode plannedMove;
        GameStateNode nextMove;

        treeRecursor(root, type, depth);
        valueFeeder(root,1);

        plannedMove = MinMaxProcess(root);

        while (plannedMove.gsp != root)
        {
            plannedMove = plannedMove.gsp;
        }
        
        return plannedMove.data;
    }

    void treeRecursor(GameStateNode branch, int type, int depth)
    {
        if (!checkVictory(branch.data))
        {
            for (int k = 0; k < 3; k++)
            {
                for (int l = 0; l < 3; l++)
                {
                    if (branch.data[k, l] == 0)
                    {
                        int[,] tempI = branch.data;
                        tempI[k, l] = type;
                        GameStateNode gsn = new GameStateNode();
                        gsn.gsp = branch;
                        gsn.ownTurn = true;
                        gsn.data = tempI;

                        branch.gsc.Add(gsn);
                    }
                }
            }
            if (depth > 0)
            {
                depth--;
                foreach (GameStateNode b in branch.gsc)
                {
                    treeRecursor(b, type, depth);
                }

            }
        }
    }

    void valueFeeder(GameStateNode branch,int mul)
    {
        if (branch.gsc.Count > 0)
        {
            foreach (GameStateNode n in branch.gsc)
            {
                n.value = heuristic(n.data);
                if (checkVictory(n.data))
                {
                    n.value = 999 * mul;
                }
                if (n.gsc.Count > 0)
                {
                    valueFeeder(n, mul * -1);
                }
            }
        }
    }

    GameStateNode MinMaxProcess(GameStateNode root)
    {
        GameStateNode temp = MaxProcess(root);
        return temp;
    }

    GameStateNode MaxProcess(GameStateNode node)
    {
        List<GameStateNode> compNode = new List<GameStateNode>();

        if (node.gsc.Count > 0)
        {
            foreach (GameStateNode nc in node.gsc)
            {
                if (nc.gsc.Count > 0)
                {
                    compNode.Add(MinProcess(nc));
                }
            }
        }
        else
        {
            return node;
        }
        return Max(compNode);
    }

    GameStateNode MinProcess(GameStateNode node)
    {
        List<GameStateNode> compNode = new List<GameStateNode>();

        if (node.gsc.Count > 0)
        {
            foreach (GameStateNode nc in node.gsc)
            {
                if (nc.gsc.Count > 0)
                {
                    compNode.Add(MaxProcess(nc));
                }
            }
        }
        else
        {
            return node;
        }

        return Min(compNode);
    }

    GameStateNode Max(List<GameStateNode> nodes)
    {
        for(int k = 0; k < nodes.Count - 1; k++)
        {
            for (int l = 0; l < nodes.Count - 1; l++)
            {
                if(nodes[l].value > nodes[l + 1].value)
                {
                    GameStateNode temp = nodes[l];
                    nodes.RemoveAt(l);
                    nodes.Insert(l + 1, temp);
                }
            }
        }
        return nodes[nodes.Count-1];
    }

    GameStateNode Min(List<GameStateNode> nodes)
    {
        for (int k = 0; k < nodes.Count - 1; k++)
        {
            for (int l = 0; l < nodes.Count - 1; l++)
            {
                if (nodes[l].value > nodes[l + 1].value)
                {
                    GameStateNode temp = nodes[l];
                    nodes.RemoveAt(l);
                    nodes.Insert(l + 1, temp);
                }
            }
        }
        return nodes[0];
    }
    
    int heuristic(int[,] move)
    {
        int tempValue = 0;
        for (int k = 0; k < 3; k++)
        {
            for (int l = 0; l < 3; l++)
            {
                if (move[k, l] != 0)
                {
                    if(k == 1 & l == 1)
                    {
                        tempValue = 4;
                    }
                    else if(k == 1 & l == 0 | k == 0 & l == 1 | k == 2 & l == 1 | k == 1 & l == 2)
                    {
                        tempValue = 2;
                    }
                    else if (k == 0 & l == 0 | k == 2 & l == 2 | k == 2 & l == 0 | k == 2 & l == 0)
                    {
                        tempValue = 3;
                    }
                }
            }
        }
                return tempValue;
    }

    public void setTrigger()
    {
        trigger = true;
    }

    public void setPosition(int pos)
    {
        if (canPlay)
        {
            Vector2Int posV = new Vector2Int(pos / 10, pos % 10);
            if (matrix[posV.x, posV.y] == 0)
            {
                if (currentPlayer == player.P1)
                {
                    if (p1O)
                    {
                        matrix[posV.x, posV.y] = -1;
                    }
                    else
                    {
                        matrix[posV.x, posV.y] = 1;
                    }
                }
                else
                {
                    if (p2O)
                    {
                        matrix[posV.x, posV.y] = -1;
                    }
                    else
                    {
                        matrix[posV.x, posV.y] = 1;
                    }
                }
                buildMatrix();
                setTrigger();

                if(currentPlayer == player.P1)
                {
                    currentPlayer = player.P2;
                }
                else if (currentPlayer == player.P2)
                {
                    currentPlayer = player.P1;
                }
                canPlay = false;
            }
        }
    }

    public void quitGame()
    {
        stateMac = state.reset;
    }
}
